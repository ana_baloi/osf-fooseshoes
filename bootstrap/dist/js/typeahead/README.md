# Show Suggestion
Show suggestion is a JQuery plugin which is used to show the suggustion using given data under given element.

# How to use
```javascript
var data = [
  {"fname":"Kumaravel", "lname" : "Palanisamy", "email": "kumar.okm1995@gmail.com", "id": 1},
  {"fname":"Test", "lname" : "Test", "email": "test@test.com", "id": 2},
  {"fname":"Sample", "lname" : "Sample", "email": "sample@sample.com", "id": 3},
  {"fname":"Demo", "lname" : "Demo", "email": "demo@demo.com", "id": 4}
];
var config = {};
config["data"] = data;
config["attr"] = ["id"];
config["searchBy"] = ["fname", "lname", "email"];
config["suggFormat"] = "$0 $1 - $2";
config["displayFormat"] = "$0 $1";

$(".assinee").showSuggestion(config);
```

# Output
![](https://lh3.googleusercontent.com/e7NWlf5mqYMQOsq70BCYGn6sjvNth9mC_PPJA_dNxux_JX9qCoryz4U2VWBUL5qP1mibiG4sf3F2lR9-PEsX=w1920-h857)

![](https://lh5.googleusercontent.com/GvW0FcWco_tYeExE4MvNPbKs60PZpcGv3HSSuhR14HUtH7qhv733LoAcX6Jh7sRbe30S-_6dAWg3Vl8y97Kx=w1920-h857)
