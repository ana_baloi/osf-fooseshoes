var suggestionList = [
    {
      "category":"Men Shoes", 
      "products" : "15 products available", 
      "id": 1
    },
    {
		"category":"Women Shoes", 
		"products" : "12 products available", 
      "id": 2
    },
    {
		"category":"Kids Shoes", 
		"products" : "9 products available", 
      "id": 3
      }
];

var config = {};
config["data"] = suggestionList;
config["attr"] = ["id"];
config["searchBy"] = ["category", "products"];
config["suggFormat"] = "$0 $1 - $2";
config["displayFormat"] = "$0 $1";

$("#search-form").showSuggestion(config);